export const authEndpoints = {
    login: '/login_check',
}

export const empresaEndpoints = {
    rest: '/empresas'
}
export const socioEndpoints = {
    rest: '/socios'
}

export const userEndpoints = {
    rest: '/users'
}