import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function validateCPF(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    let value = control.value;
    let cpf_Valid = true;

    if (!value) {
        console.log(1);
      return null;
    }

    value = value.replace(/[^\d]/g, '');

    if (value.length !== 11 || /^(.)\1+$/.test(value)) {
        console.log(2);
        cpf_Valid = false;
    }

    let sum = 0;
    let rest;

    for (let i = 1; i <= 9; i++) {
      sum += parseInt(value.substring(i - 1, i)) * (11 - i);
    }

    rest = (sum * 10) % 11;

    if (rest === 10 || rest === 11) {
      rest = 0;
    }

    if (rest !== parseInt(value.substring(9, 10))) {
        console.log(3);
        cpf_Valid = false;
    }

    sum = 0;

    for (let i = 1; i <= 10; i++) {
      sum += parseInt(value.substring(i - 1, i)) * (12 - i);
    }

    rest = (sum * 10) % 11;

    if (rest === 10 || rest === 11) {
      rest = 0;
    }

    if (rest !== parseInt(value.substring(10, 11))) {
        console.log(4);
        cpf_Valid = false;
    }

    return !cpf_Valid ? { cpfValid: false } : null;
  };
}
