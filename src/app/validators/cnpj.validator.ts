import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function validateCNPJ(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    let value = control.value;
    let cnpj_Valid = true;

    if (!value) {
      return null;
    }

    value = value.replace(/[^\d]/g, '');

    if (value.length !== 14 || /^(.)\1+$/.test(value)) {
      cnpj_Valid = false;
    }

    let tamanho = value.length - 2;
    let numeros = value.substring(0, tamanho);
    const digitos = value.substring(tamanho);
    let soma = 0;
    let pos = tamanho - 7;

    for (let i = tamanho; i >= 1; i--) {
      soma += parseInt(numeros.charAt(tamanho - i)) * pos--;
      if (pos < 2) {
        pos = 9;
      }
    }

    let resultado = soma % 11 < 2 ? 0 : 11 - (soma % 11);

    if (resultado !== parseInt(digitos.charAt(0))) {
      cnpj_Valid = false;
    }

    tamanho += 1;
    numeros = value.substring(0, tamanho);
    soma = 0;
    pos = tamanho - 7;

    for (let i = tamanho; i >= 1; i--) {
      soma += parseInt(numeros.charAt(tamanho - i)) * pos--;
      if (pos < 2) {
        pos = 9;
      }
    }

    resultado = soma % 11 < 2 ? 0 : 11 - (soma % 11);

    if (resultado !== parseInt(digitos.charAt(1))) {
      cnpj_Valid = false;
    }

    return !cnpj_Valid ? { cnpjValid: false } : null;
  };
}
