import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import {MatIconModule} from '@angular/material/icon';
import { AuthService } from '../../../auth/services/auth.service';

@Component({
  selector: 'app-dialog',
  standalone: true,
  imports: [CommonModule,MatIconModule],
  templateUrl: './dialog.component.html',
  styleUrl: './dialog.component.scss'
})
export class DialogComponent {
  @Input() title!:string
  @Input() message!:string
  @Input() show!:boolean
  isLoggedIn!: boolean;
  
  constructor () {}

  close(){
    this.show = false;
  }

}
