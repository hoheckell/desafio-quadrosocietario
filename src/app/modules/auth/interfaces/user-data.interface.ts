export interface IUserData
{
    iat: number
    exp: number
    roles: string[]
    username: string
}