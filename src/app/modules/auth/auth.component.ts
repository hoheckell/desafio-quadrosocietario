import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './services/auth.service';
import { iLoginResponse } from './interfaces/login-response.interface';
import { DialogComponent } from '../shared/components/dialog/dialog.component';
import { Router } from '@angular/router';
import { ConfigService } from './services/config.service';
import * as jwt_decode from 'jwt-decode';
import { IUserData } from './interfaces/user-data.interface';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-auth',
  standalone: true,
  imports: [FormsModule, ReactiveFormsModule, DialogComponent],
  providers:[CookieService],
  templateUrl: './auth.component.html',
  styleUrl: './auth.component.scss',
})
export class AuthComponent {
  formLogin!: FormGroup;
  titleModal!: string;
  messageModal!: string;
  showModal = false;
  constructor(
    private fb: FormBuilder,
    private authservice: AuthService,
    private router: Router,
    private config: ConfigService,
    private cookieService: CookieService
  ) {
    this.formLogin = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  setModal(title: string, message: string) {
    this.titleModal = title;
    this.messageModal = message;
    this.showModal = true;
  }

  onSubmit() {
    if (this.formLogin.valid) {
      this.authservice.login(this.formLogin.value).subscribe({
        next: (response: iLoginResponse) => {
          this.cookieService.set('token', response.token,3600,"/"); 
          const payload = jwt_decode.jwtDecode<IUserData>(response.token); 
          this.config.userData.next(payload);
          this.router.navigate(['/dashboard'])
        },
        error: (error) => {
          this.setModal('Erro', error.error.response);
        },
      });
    }
  }

  ngOnInit(): void { 
    if (this.cookieService.get('token')) {
      this.router.navigate(['/dashboard'])
    }
  }
}
