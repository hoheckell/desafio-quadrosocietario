import { HttpClient, HttpBackend, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, take } from 'rxjs';
import { ILoginData } from '../interfaces/login-data.interface';
import { iLoginResponse } from '../interfaces/login-response.interface';
import { environment } from '../../../../environments/environment';
import { authEndpoints } from '../../../../config/endpoints';
import { ConfigService } from './config.service';
import { IUserData } from '../interfaces/user-data.interface';
import moment from 'moment';
import { Router } from '@angular/router';
import * as jwt_decode from 'jwt-decode';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private http: HttpClient;

  constructor(
    private handler: HttpBackend,
    private httpRequest: HttpClient,
    private config: ConfigService,
    private router: Router,
    private cookieService: CookieService
  ) {
    this.http = new HttpClient(handler);
  }

  login(loginData: ILoginData): Observable<iLoginResponse> {
    return this.httpRequest
      .post<iLoginResponse>(environment.API + authEndpoints.login, loginData)
      .pipe(take(3));
  }

  isLogged() {
    if (typeof window !== 'undefined') {
      const token = this.cookieService.get('token');
      if (token) {
        const { exp } = jwt_decode.jwtDecode<IUserData>(token);
        var momentoDaData = moment.unix(exp);
        if (!momentoDaData.isAfter(moment())) {
          this.cookieService.set('token','',1,"/") 
          this.cookieService.deleteAll("/") 
          this.config.userData.next({});
          this.router.navigate(['/']);
        }
        return true;
      } else {
        this.router.navigate(['/']);
      }
    }
    return false;
  }
}
