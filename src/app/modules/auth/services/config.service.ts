import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
//import { iUserData } from '../../auth/interfaces/auth.interfaces'; 

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  isLoading = new BehaviorSubject<boolean>(true);
  userData = new BehaviorSubject<any | null>(null);
  userImage = new BehaviorSubject<string>(''); 


  constructor() { }

}


