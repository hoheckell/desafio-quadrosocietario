import { Component } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { CommonModule } from '@angular/common';
import { Router, RouterModule, RouterOutlet } from '@angular/router';
import { AuthService } from '../../../auth/services/auth.service';
import { ConfigService } from '../../../auth/services/config.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-dashboard',
  standalone: true,
  imports: [CommonModule, MatIconModule, RouterOutlet, RouterModule],
  providers:[CookieService],
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.scss',
})
export class DashboardComponent {
  showmenu = false;
  mobilemenushow = false;
  actualRoute = '';
  constructor(
    private router: Router,
    private authservice: AuthService,
    private config: ConfigService,
    private cookieService: CookieService
  ) {
    this.actualRoute = this.router.url;
    console.log(this.actualRoute);
  }

  openMenu() {
    this.showmenu = !this.showmenu;
  }

  sair() {        
    this.cookieService.set('token',"",1,"/")  
    this.cookieService.deleteAll("/") 
    this.config.userData.next({});
    this.router.navigate(['/']);
  }

  ngOnInit(): void {
    this.actualRoute = this.router.url;
    console.log(this.router.url);
    this.authservice.isLogged();
  }

  showMenu() {
    this.mobilemenushow = !this.mobilemenushow;
  }
}
