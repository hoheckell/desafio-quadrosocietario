import { HttpClient, HttpBackend, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, take } from 'rxjs';
import {
  empresaEndpoints,
  socioEndpoints,
} from '../../../../../../config/endpoints';
import { environment } from '../../../../../../environments/environment';
import { ISocio } from '../interfaces/socio.interface';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root',
})
export class SocioService {
  private http: HttpClient;
  myToken!: string;
  constructor(
    private handler: HttpBackend,
    private httpRequest: HttpClient,
    private cookieService: CookieService
  ) {
    this.http = new HttpClient(handler);
    this.myToken = this.cookieService.get('token');
  }

  ngOnInit() {}

  getSocios(): Observable<ISocio[]> {
    return this.httpRequest
      .get<ISocio[]>(environment.API + socioEndpoints.rest, {
        headers: {
          Authorization: `Bearer ${this.myToken}`,
        },
      })
      .pipe(take(3));
  }

  getSocio(id: number): Observable<ISocio> {
    return this.httpRequest
      .get<ISocio>(environment.API + socioEndpoints.rest + `/${id}`, {
        headers: {
          Authorization: `Bearer ${this.myToken}`,
        },
      })
      .pipe(take(3));
  }

  create(socio: ISocio): Observable<ISocio> {
    return this.httpRequest
      .post<ISocio>(environment.API + socioEndpoints.rest, socio, {
        headers: {
          Authorization: `Bearer ${this.myToken}`,
        },
      })
      .pipe(take(3));
  }

  update(socio: ISocio, id: number): Observable<void> {
    return this.httpRequest
      .put<void>(environment.API + socioEndpoints.rest + `/${id}`, socio, {
        headers: {
          Authorization: `Bearer ${this.myToken}`,
        },
      })
      .pipe(take(3));
  }

  delete(id: number): Observable<void> {
    return this.httpRequest
      .delete<void>(environment.API + socioEndpoints.rest + `/${id}`, {
        headers: {
          Authorization: `Bearer ${this.myToken}`,
        },
      })
      .pipe(take(3));
  }

  desassociar(idsocio: number, idempresa: number) {
    return this.httpRequest
      .delete<void>(
        environment.API +
          empresaEndpoints.rest +
          `/${idempresa}/socios/${idsocio}`,
        {
          headers: {
            Authorization: `Bearer ${this.myToken}`,
          },
        }
      )
      .pipe(take(3));
  }
}
