import { CommonModule } from '@angular/common';
import { Component, ViewChild } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { RouterModule, RouterLink } from '@angular/router';
import { AlertaComponent } from '../../../shared/components/alerta/alerta.component';
import { EmpresaService } from '../empresas/services/empresa.service';
import { SocioService } from './services/socio.service';
import { IEmpresa } from '../empresas/interfaces/empresa.interface';
import { ISocio } from './interfaces/socio.interface';

@Component({
  selector: 'app-socios',
  standalone: true,
  imports: [
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    AlertaComponent,
    RouterModule,
    RouterLink,
    CommonModule,],
  templateUrl: './socios.component.html',
  styleUrl: './socios.component.scss'
})
export class SociosComponent {
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private empresaService: EmpresaService,
    private socioService: SocioService
  ) {}
  type!: string;
  msg!: string;
  showAlertMessage = false;
  socios!: ISocio[];
  empresaslist!: IEmpresa[];
  showSociosModal = false;
  socioId!: number
  displayedColumns: string[] = [
    'name',
    'email',
    'cpf',
    'actions',
  ];
  dataSource = new MatTableDataSource<ISocio>(this.socios);

  showAlert(type: string, message: string, show: boolean) {
    this.msg = message;
    this.type = type;
    this.showAlertMessage = show;
    setTimeout(() => {
      this.showAlertMessage = false;
    }, 3000);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  getAll() {
    this.socioService.getSocios().subscribe({
      next: (response: ISocio[]) => {
        this.socios = response;
      },
      error: () => {},
    });
  }
  ngOnInit() {
    this.getAll();
  }

  delete(id: number) {
    this.socioService.delete(id).subscribe({
      next: () => {
        this.showAlert('Sucesso', 'Sócio excluido com sucesso', true);
        this.getAll();
      },
      error: () => {
        this.showAlert('Erro', 'Houve um erro ao excluir sócio', true);
      },
    });
  }

  listaEmpresas(idsocio: any, empresas: IEmpresa[]) {
    this.empresaslist = empresas;
    this.socioId = idsocio 
    this.showSociosModal = true;
  }

  desfiliar( idsocio:number,idempresa: any) {
    if(idempresa && idsocio)
    this.socioService.desassociar(idsocio,idempresa).subscribe({
      next: () => {
        this.showAlert('Sucesso', 'Desassociado com sucesso', true);
        this.getAll();
      },
      error: () => {
        this.showAlert('Erro', 'Houve um erro ao desassociar', true);
      },
    });
  }
}
