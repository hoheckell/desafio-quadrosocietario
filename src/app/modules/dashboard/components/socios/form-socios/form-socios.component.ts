import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, Component } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, ValidationErrors, Validators } from '@angular/forms';
import { AlertaComponent } from '../../../../shared/components/alerta/alerta.component';
import { ISocio } from '../interfaces/socio.interface';
import { ActivatedRoute } from '@angular/router';
import { SocioService } from '../services/socio.service';
import { validateCPF } from '../../../../../validators/cpf.validator';

@Component({
  selector: 'app-form-socios',
  standalone: true,
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [FormsModule, ReactiveFormsModule, CommonModule, AlertaComponent],
  templateUrl: './form-socios.component.html',
  styleUrl: './form-socios.component.scss'
})
export class FormSociosComponent {
  socioid!: number;
  socio!: ISocio;
  actionFormGroup!: FormGroup;
  edit = false;
  type!: string;
  msg!: string;
  avisar = false;
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private socioService: SocioService
  ) {
    this.actionFormGroup = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      cpf: ['', [Validators.required, validateCPF()]],
    });
  }

  showAlert(type: string, message: string, show: boolean) {
    this.msg = message;
    this.type = type;
    this.avisar = show;
    setTimeout(() => {
      this.avisar = false;
    }, 3000);
  }

  onSubmit() {
    console.log(this.actionFormGroup.value)
    if (this.actionFormGroup.valid && this.edit) {
      this.socioService
        .update(this.actionFormGroup.value, this.socioid)
        .subscribe({
          next: () => {
            this.showAlert('Sucesso', 'Empresa Alterada com sucesso', true);
          },
          error: (error) => {
            this.showAlert('Erro', error.error.response, true);
          },
        });
    } else if (this.actionFormGroup.valid && !this.edit) {
      this.socioService.create(this.actionFormGroup.value).subscribe({
        next: (response: ISocio) => {
          this.showAlert('Sucesso', 'Empresa Criada com sucesso', true);
        },
        error: (error) => {
          this.showAlert('Erro', error.error.response, true);
        },
      });
    } else {
      this.showAlert('Erro', this.getFormValidationErrors(), true);
    }
  }

  ngOnInit() {
    this.socioid = Number(this.route.snapshot.paramMap.get('id')) ?? null;
    if (this.socioid) {
      this.edit = true;
      this.socioService.getSocio(this.socioid).subscribe({
        next: (response: ISocio) => {
          this.actionFormGroup = this.fb.group({
            name: [
              response.name, Validators.required,
            ],
            email: [response.email, [Validators.required, Validators.email]],
            cpf: [response.cpf, [Validators.required, validateCPF()]],
          });
        },
        error: (error) => {
          console.log(error.error.response);
        },
      });
    }
  }

  getFormValidationErrors() {
    let message = '';
    Object.keys(this.actionFormGroup.controls).forEach((key) => {
      if (this.actionFormGroup.get(key)) {
        const controlErrors: ValidationErrors =
          this.actionFormGroup.get(key)?.errors ?? {};
        if (Object.keys(controlErrors).length !== 0) {
          Object.keys(controlErrors).forEach((keyError) => {
            message +=
              key + ' ' + keyError + ', err value: ' + controlErrors[keyError];
          });
        }
      }
    });
    return message;
  }
}
