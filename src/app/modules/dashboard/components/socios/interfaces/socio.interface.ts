import { IEmpresa } from "../../empresas/interfaces/empresa.interface";

export interface ISocio
{
    id?:number;
    name:string
    email:string
    cpf:string
    empresas?:IEmpresa[]
}