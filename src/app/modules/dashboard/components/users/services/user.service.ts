import { HttpClient, HttpBackend, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, take } from 'rxjs';
import {
  empresaEndpoints,
  socioEndpoints,
  userEndpoints,
} from '../../../../../../config/endpoints';
import { environment } from '../../../../../../environments/environment';
import { IUser } from '../interfaces/user.interface';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private http: HttpClient;
  myToken!: string;
  constructor(
    private handler: HttpBackend,
    private httpRequest: HttpClient,
    private cookieService: CookieService
  ) {
    this.http = new HttpClient(handler);
    this.myToken = this.cookieService.get('token');
  }

  ngOnInit() {}

  getUsers(): Observable<IUser[]> {
    return this.httpRequest
      .get<IUser[]>(environment.API + userEndpoints.rest, {
        headers: {
          Authorization: `Bearer ${this.myToken}`,
        },
      })
      .pipe(take(3));
  }

  getUser(id: number): Observable<IUser> {
    return this.httpRequest
      .get<IUser>(environment.API + userEndpoints.rest + `/${id}`, {
        headers: {
          Authorization: `Bearer ${this.myToken}`,
        },
      })
      .pipe(take(3));
  }

  update(user: IUser, id: number): Observable<void> {
    return this.httpRequest
      .put<void>(environment.API + userEndpoints.rest + `/${id}`, user, {
        headers: {
          Authorization: `Bearer ${this.myToken}`,
        },
      })
      .pipe(take(3));
  }

  create(user: IUser): Observable<IUser> {
    return this.httpRequest
      .post<IUser>(environment.API + userEndpoints.rest, user, {
        headers: {
          Authorization: `Bearer ${this.myToken}`,
        },
      })
      .pipe(take(3));
  }

  delete(id: number) {
    return this.httpRequest
      .delete<IUser>(environment.API + userEndpoints.rest + `/${id}`, {
        headers: {
          Authorization: `Bearer ${this.myToken}`,
        },
      })
      .pipe(take(3));
  }
}
