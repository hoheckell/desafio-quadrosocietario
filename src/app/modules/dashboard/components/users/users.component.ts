import { CUSTOM_ELEMENTS_SCHEMA, Component, ViewChild } from '@angular/core';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { UserService } from './services/user.service';
import { IUser } from './interfaces/user.interface';
import { MatIconModule } from '@angular/material/icon';
import { RouterLink, RouterModule } from '@angular/router';
import { AlertaComponent } from '../../../shared/components/alerta/alerta.component';
@Component({
  selector: 'app-users',
  standalone: true,
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    RouterModule,
    RouterLink,
    AlertaComponent
  ],
  templateUrl: './users.component.html',
  styleUrl: './users.component.scss',
})
export class UsersComponent {
  users!: IUser[];
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(private userService: UserService) {}

  type!: string;
  msg!: string;
  showAlertMessage = false

  displayedColumns: string[] = ['name', 'email', 'actions'];
  dataSource = new MatTableDataSource<IUser>(this.users);

  showAlert(type: string, message: string, show: boolean){
    this.msg = message
    this.type = type
    this.showAlertMessage = show
    setTimeout(()=>{
      this.showAlertMessage = false
    },3000)
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
  getAll(){
    this.userService.getUsers().subscribe({
      next: (response: IUser[]) => {
        this.users = response;
      },
      error: () => {},
    });
  }
  ngOnInit() {
    this.getAll()
  }

  delete(id: number) {
    this.userService.delete(id).subscribe({
      next: () => {
        this.showAlert("Sucesso","Usuário excluido com sucesso",true)
        this.getAll()
      },
      error: () => {
        this.showAlert("Erro","Houve um erro ao excluir usuários",true)
      },
    });
  }
}
