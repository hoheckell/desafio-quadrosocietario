import { CUSTOM_ELEMENTS_SCHEMA, Component } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IUser } from '../interfaces/user.interface';
import { UserService } from '../services/user.service';
import { CommonModule } from '@angular/common';
import { AlertaComponent } from '../../../../shared/components/alerta/alerta.component';

@Component({
  selector: 'app-form-users',
  standalone: true,
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [FormsModule, ReactiveFormsModule, CommonModule, AlertaComponent],
  templateUrl: './form-users.component.html',
  styleUrl: './form-users.component.scss',
})
export class FormUsersComponent {
  userid!: number;
  user!: IUser;
  actionFormGroup!: FormGroup;
  edit = false;
  type!: string;
  msg!: string;
  avisar = false;
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private userservice: UserService
  ) {
    this.actionFormGroup = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      name: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  showAlert(type: string, message: string, show: boolean) {
    this.msg = message;
    this.type = type;
    this.avisar = show;
    setTimeout(() => {
      this.avisar = false;
    }, 3000);
  }

  onSubmit() { 
    if (this.actionFormGroup.valid && this.edit) {
      this.userservice
        .update(this.actionFormGroup.value, this.userid)
        .subscribe({
          next: () => {
            this.showAlert('Sucesso', 'Usuário Alterado com sucesso', true);
          },
          error: (error) => {
            this.showAlert('Erro', error.error.response, true);
          },
        });
    } else if (this.actionFormGroup.valid && !this.edit) {
      this.userservice.create(this.actionFormGroup.value).subscribe({
        next: (response: IUser) => {
          this.showAlert('Sucesso', 'Usuário Criado com sucesso', true);
        },
        error: (error) => {
          this.showAlert('Erro', error.error.response, true);
        },
      });
    }
  }

  ngOnInit() {
    this.userid = Number(this.route.snapshot.paramMap.get('id')) ?? null;
    if (this.userid) {
      this.edit = true;
      this.userservice.getUser(this.userid).subscribe({
        next: (response: IUser) => {
          this.actionFormGroup = this.fb.group({
            email: [response.email, [Validators.required, Validators.email]],
            name: [response.name, Validators.required],
          });
        },
        error: (error) => {
          console.log(error.error.response);
        },
      });
    }
  }
}
