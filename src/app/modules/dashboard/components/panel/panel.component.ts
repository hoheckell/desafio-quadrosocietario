import { Component, Input } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { RouterModule } from '@angular/router';
import { EmpresaService } from '../empresas/services/empresa.service';
import { CommonModule } from '@angular/common';
import { IEmpresa } from '../empresas/interfaces/empresa.interface';
import { SocioService } from '../socios/services/socio.service';
import { ISocio } from '../socios/interfaces/socio.interface';
import { UserService } from '../users/services/user.service';
import { IUser } from '../users/interfaces/user.interface';

@Component({
  selector: 'app-panel',
  standalone: true,
  imports: [MatCardModule, RouterModule, CommonModule],
  templateUrl: './panel.component.html',
  styleUrl: './panel.component.scss',
})
export class PanelComponent {
  sociosTotal = 10;
  empresasTotal = 10;
  usersTotal = 10;

  constructor(
    private empresaService: EmpresaService,
    private socioService: SocioService,
    private userService: UserService
    ) {}

  ngOnInit() {
    this.empresaService.getEmpresas().subscribe({
      next: (response: IEmpresa[]) => {
        this.empresasTotal = response.length
      },
      error: () => {},
    });
    this.socioService.getSocios().subscribe({
      next: (response: ISocio[]) => {
        this.sociosTotal = response.length
      },
      error: () => {},
    });
    this.userService.getUsers().subscribe({
      next: (response: IUser[]) => {
        this.usersTotal = response.length
      },
      error: () => {},
    });
  }
}
