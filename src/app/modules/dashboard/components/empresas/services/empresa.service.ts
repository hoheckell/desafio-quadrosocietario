import { HttpClient, HttpBackend, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, take } from 'rxjs';
import { empresaEndpoints } from '../../../../../../config/endpoints';
import { environment } from '../../../../../../environments/environment';
import { IEmpresa } from '../interfaces/empresa.interface';
import { CookieService } from 'ngx-cookie-service';
import { ISocio } from '../../socios/interfaces/socio.interface';

@Injectable({
  providedIn: 'root',
})
export class EmpresaService {
  private http: HttpClient;
  myToken!: string;

  constructor(
    private handler: HttpBackend,
    private httpRequest: HttpClient,
    private cookieService: CookieService
  ) {
    this.http = new HttpClient(handler);
    this.myToken = this.cookieService.get('token');
  }

  ngOnInit() {}

  getEmpresas(): Observable<IEmpresa[]> {
    return this.httpRequest
      .get<IEmpresa[]>(environment.API + empresaEndpoints.rest, {
        headers: {
          Authorization: `Bearer ${this.myToken}`,
        },
      })
      .pipe(take(3));
  }

  getEmpresa(id: number): Observable<IEmpresa> {
    return this.httpRequest
      .get<IEmpresa>(environment.API + empresaEndpoints.rest + `/${id}`, {
        headers: {
          Authorization: `Bearer ${this.myToken}`,
        },
      })
      .pipe(take(3));
  }

  update(empresa: IEmpresa, id: number): Observable<void> {
    return this.httpRequest
      .put<void>(environment.API + empresaEndpoints.rest + `/${id}`, empresa, {
        headers: {
          Authorization: `Bearer ${this.myToken}`,
        },
      })
      .pipe(take(3));
  }

  delete(id: number): Observable<void> {
    return this.httpRequest
      .delete<void>(environment.API + empresaEndpoints.rest + `/${id}`, {
        headers: {
          Authorization: `Bearer ${this.myToken}`,
        },
      })
      .pipe(take(3));
  }

  create(empresa: IEmpresa): Observable<IEmpresa> {
    return this.httpRequest
      .post<IEmpresa>(environment.API + empresaEndpoints.rest, empresa, {
        headers: {
          Authorization: `Bearer ${this.myToken}`,
        },
      })
      .pipe(take(3));
  }

  addSocio(socio: ISocio, empresaid: number) {
    return this.httpRequest
      .post<void>(environment.API + empresaEndpoints.rest + `/${empresaid}/socios/`, socio, {
        headers: {
          Authorization: `Bearer ${this.myToken}`,
        },
      })
      .pipe(take(3));
  }
}
