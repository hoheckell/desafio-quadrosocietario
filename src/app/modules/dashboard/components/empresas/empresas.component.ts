import { Component, ViewChild } from '@angular/core';
import { AlertaComponent } from '../../../shared/components/alerta/alerta.component';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { RouterModule, RouterLink } from '@angular/router';
import { EmpresaService } from './services/empresa.service';
import { IEmpresa } from './interfaces/empresa.interface';
import { ISocio } from '../socios/interfaces/socio.interface';
import { CommonModule } from '@angular/common';
import { SocioService } from '../socios/services/socio.service';

@Component({
  selector: 'app-empresas',
  standalone: true,
  imports: [
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    AlertaComponent,
    RouterModule,
    RouterLink,
    CommonModule,
  ],
  templateUrl: './empresas.component.html',
  styleUrl: './empresas.component.scss',
})
export class EmpresasComponent {
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private empresaService: EmpresaService,
    private socioService: SocioService
  ) {}
  type!: string;
  msg!: string;
  showAlertMessage = false;
  empresas!: IEmpresa[];
  socioslist!: ISocio[];
  showSociosModal = false;
  displayedColumns: string[] = [
    'razao_social',
    'nome_fantasia',
    'cnpj',
    'actions',
  ];
  dataSource = new MatTableDataSource<IEmpresa>(this.empresas);

  showAlert(type: string, message: string, show: boolean) {
    this.msg = message;
    this.type = type;
    this.showAlertMessage = show;
    setTimeout(() => {
      this.showAlertMessage = false;
    }, 3000);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  getAll() {
    this.empresaService.getEmpresas().subscribe({
      next: (response: IEmpresa[]) => {
        this.empresas = response;
      },
      error: () => {},
    });
  }
  ngOnInit() {
    this.getAll();
  }

  delete(id: number) {
    this.empresaService.delete(id).subscribe({
      next: () => {
        this.showAlert('Sucesso', 'Empresa excluida com sucesso', true);
        this.getAll();
      },
      error: () => {
        this.showAlert('Erro', 'Houve um erro ao excluir empresa', true);
      },
    });
  }
  socios(socios: ISocio[]) {
    this.socioslist = socios;
    this.showSociosModal = true;
  }

  deleteSocio(id: any) {
    this.socioService.delete(id).subscribe({
      next: () => {
        this.showAlert('Sucesso', 'Socio excluido com sucesso', true);
        this.getAll();
      },
      error: () => {
        this.showAlert('Erro', 'Houve um erro ao excluir sócio', true);
      },
    });
  }
}
