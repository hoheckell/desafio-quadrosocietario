import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, Component } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { AlertaComponent } from '../../../../shared/components/alerta/alerta.component';
import { IEmpresa } from '../interfaces/empresa.interface';
import { ActivatedRoute } from '@angular/router';
import { EmpresaService } from '../services/empresa.service';
import { validateCNPJ } from '../../../../../validators/cnpj.validator';
import { validateCPF } from '../../../../../validators/cpf.validator';

@Component({
  selector: 'app-form-empresas',
  standalone: true,
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [FormsModule, ReactiveFormsModule, CommonModule, AlertaComponent],
  templateUrl: './form-empresas.component.html',
  styleUrl: './form-empresas.component.scss',
})
export class FormEmpresasComponent {
  empresaid!: number;
  empresa!: IEmpresa;
  actionFormGroup!: FormGroup;
  socioFormGroup!: FormGroup
  edit = false;
  type!: string;
  msg!: string;
  avisar = false;
  showSocioForm = false
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private empresaService: EmpresaService
  ) {
    this.actionFormGroup = this.fb.group({
      razao_social: ['', Validators.required],
      nome_fantasia: ['', Validators.required],
      cnpj: ['', [Validators.required, validateCNPJ()]],
    });
    this.socioFormGroup = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      cpf: ['', [Validators.required, validateCPF()]],
    });
  }

  showAlert(type: string, message: string, show: boolean) {
    this.msg = message;
    this.type = type;
    this.avisar = show;
    setTimeout(() => {
      this.avisar = false;
    }, 3000);
  }

  onSubmit() { 
    if (this.actionFormGroup.valid && this.edit) {
      this.empresaService
        .update(this.actionFormGroup.value, this.empresaid)
        .subscribe({
          next: () => {
            this.showAlert('Sucesso', 'Empresa Alterada com sucesso', true);
          },
          error: (error) => {
            this.showAlert('Erro', error.error.response, true);
          },
        });
    } else if (this.actionFormGroup.valid && !this.edit) {
      this.empresaService.create(this.actionFormGroup.value).subscribe({
        next: (response: IEmpresa) => {
          this.showAlert('Sucesso', 'Empresa Criada com sucesso', true);
        },
        error: (error) => {
          this.showAlert('Erro', error.error.response, true);
        },
      });
    } else {
      this.showAlert('Erro', this.getFormValidationErrors(), true);
    }
  }

  saveSocio(){
    if (this.socioFormGroup.valid && this.edit) {
      this.empresaService
        .addSocio(this.socioFormGroup.value, this.empresaid)
        .subscribe({
          next: () => {
            this.showAlert('Sucesso', 'Socio Adicionado com sucesso', true);
          },
          error: (error) => {
            this.showAlert('Erro', error.error.response, true);
          },
        });
    } else {
      this.showAlert('Erro', this.getFormValidationErrors(), true);
    }
  }

  ngOnInit() {
    this.empresaid = Number(this.route.snapshot.paramMap.get('id')) ?? null;
    if (this.empresaid) {
      this.edit = true;
      this.empresaService.getEmpresa(this.empresaid).subscribe({
        next: (response: IEmpresa) => {
          this.actionFormGroup = this.fb.group({
            razao_social: [
              response.razao_social, Validators.required,
            ],
            nome_fantasia: [response.nome_fantasia, Validators.required],
            cnpj: [response.cnpj, [Validators.required, validateCNPJ()]],
          });
        },
        error: (error) => {
          console.log(error.error.response);
        },
      });
    }
  }

  getFormValidationErrors() {
    let message = '';
    Object.keys(this.actionFormGroup.controls).forEach((key) => {
      if (this.actionFormGroup.get(key)) {
        const controlErrors: ValidationErrors =
          this.actionFormGroup.get(key)?.errors ?? {};
        if (Object.keys(controlErrors).length !== 0) {
          Object.keys(controlErrors).forEach((keyError) => {
            message +=
              key + ' ' + keyError + ', err value: ' + controlErrors[keyError];
          });
        }
      }
    });
    return message;
  }
  addsocio(){
    this.showSocioForm = true
  }
}
