import { ISocio } from "../../socios/interfaces/socio.interface";

export interface IEmpresa
{
    id?:number
    razao_social: string
    nome_fantasia?:string;
    cnpj:string;
    socios?: ISocio[]

}