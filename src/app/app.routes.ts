import { Routes } from '@angular/router';
import { AuthComponent } from './modules/auth/auth.component';
import { DashboardComponent } from './modules/dashboard/components/dashboard/dashboard.component';
import { PanelComponent } from './modules/dashboard/components/panel/panel.component';
import { SociosComponent } from './modules/dashboard/components/socios/socios.component';
import { EmpresasComponent } from './modules/dashboard/components/empresas/empresas.component';
import { UsersComponent } from './modules/dashboard/components/users/users.component';
import { FormUsersComponent } from './modules/dashboard/components/users/form-users/form-users.component';
import { FormSociosComponent } from './modules/dashboard/components/socios/form-socios/form-socios.component';
import { FormEmpresasComponent } from './modules/dashboard/components/empresas/form-empresas/form-empresas.component';

export const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: AuthComponent },
  {
    path: 'dashboard',
    component: DashboardComponent,
    children: [
      { path: '', component: PanelComponent },
      { path: 'socios', component: SociosComponent },
      { path: 'empresas', component: EmpresasComponent },
      { path: 'users', component: UsersComponent },
      { path: 'users/action/:id', component: FormUsersComponent },
      { path: 'users/action', component: FormUsersComponent },
      { path: 'socios/action/:id', component: FormSociosComponent },
      { path: 'socios/action', component: FormSociosComponent },
      { path: 'empresas/action/:id', component: FormEmpresasComponent },
      { path: 'empresas/action', component: FormEmpresasComponent },
    ],
  },
];
